#-------------------------------------------------
#
# Project created by QtCreator 2014-10-14T23:54:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kod
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    functional.cpp

HEADERS  += mainwindow.h \
    functional.h

FORMS    += mainwindow.ui
